<?php

$lang['account_import_app_description'] = 'アカウントインポートアプリは、カンマ区切り値 (CSV) 形式でアップロードされたファイルを使用してアカウントの一括インポートを実行します。';
$lang['account_import_app_name'] = 'アカウントインポート';
$lang['account_import_clear'] = 'クリア';
$lang['account_import_csv_file'] = 'CSV ファイル';
$lang['account_import_csv_file_not_found'] = 'CSV ファイルが見つかりません。';
$lang['account_import_download_csv_template'] = 'CSVテンプレートをダウンロード';
$lang['account_import_import_already_in_progress'] = 'インポートは既に進行中です。';
$lang['account_import_import_complete'] = 'インポートが完了しました。';
$lang['account_import_import_log'] = 'ログのインポート';
$lang['account_import_import_users'] = 'ユーザーのインポート';
$lang['account_import_importing_user'] = 'ユーザーをインポートする';
$lang['account_import_no_data_found'] = 'データが見つかりません。';
$lang['account_import_no_entries'] = 'インポートエントリーは見つかりませんでした。';
$lang['account_import_number_of_records'] = 'レコード数';
$lang['account_import_start_import'] = 'インポートを開始する';
$lang['account_import_tooltip_csv'] = 'CSVファイルが正しくフォーマットされていることを確認してください。値は引用符で囲み、カンマで区切ります（"value1"、"value2 "など）。';
$lang['account_import_unable_to_determine_running_state'] = 'インポートが実行されているかどうか判断できない。';
$lang['account_import_upload_csv_file'] = 'CSV ファイルのアップロード';
